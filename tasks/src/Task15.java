import java.util.Scanner;
public class Task15 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long n = in.nextLong();
        if (n>=-273L && n<=0)
            System.out.println("Ice");
        else if (n>=1l && n<=100000000)
            System.out.println("Water");
        else if (n<-273L)
            System.out.println("Невозможная температура");
    }
}
