import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if (n >= 100 && n <= 999)
            System.out.println(n / 100 + " " + (n / 10) % 10 + " " + n % 10);
         else
            System.out.println("Число должно быть трехзначным.");

    }
}
