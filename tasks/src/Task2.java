import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if (n>=10 && n<99)
        System.out.println(n/10+n%10+" "+n/10*n%10);
        else
            System.out.println("Введите двухзначное число");

    }
}
